var path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = (env, options) => {
    const isDevMode = options.mode ==="dev";
    return {
        entry: "./src/index.jsx",
        output: {
            path: path.join(__dirname, 'dist'),
            filename: 'bundle.js'
        },
        module: {
            rules: [
                {
                    test: /\.(js|jsx)$/,
                    exclude: '/node_modules/',
                    use: {
                        loader: 'babel-loader'
                    }
                },
                {
                    test: /\.s?css$/,
                    use: [
                        "style-loader",
                        "css-loader",
                        "sass-loader"
                    ]
                },
                {
                    test: /\.(jpe?g|png|gif|svg|ico)/i,
                    use: [
                        "file-loader"
                    ]
                }
            ]
        },
        resolve: {
            extensions: ['.', '.js', '.jsx']
        },
        plugins: [
            new HtmlWebpackPlugin({
                template: './index.html'
            })
        ],
        devServer: {
            contentBase: path.join(__dirname, 'dist'),
            compress: true,
            port: 9000,
            historyApiFallback: true
        }

    }
}