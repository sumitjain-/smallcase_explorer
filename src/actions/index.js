export function addObject(newObj){
    return {
        type: "ADD_OBJECT",
        payload: newObj
    }
}

export function deleteObject(newObj){
    return {
        type: "DELETE_OBJECT",
        payload: newObj
    }
}

export function setCurrentPath(folder){
    return {
        type: "SET_CURRENT_PATH",
        payload: folder
    }
}

export function setInfoObject(obj){
    return {
        type: "SET_INFO_OBJECT",
        payload: obj
    }
}

export function closeInfoWindow(obj){
    return {
        type: "CLOSE_INFO_WINDOW"
    }
}

export function setContextMenuObject(obj){
    return {
        type: "SET_CONTEXT_MENU_OBJECT",
        payload: obj
    }
}

export function closeContextMenuWindow(obj){
    return {
        type: "CLOSE_CONTEXT_MENU_WINDOW"
    }
}
