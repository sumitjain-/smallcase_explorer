export default function (state=null, action){
    switch(action.type){
        case "ADD_OBJECT":
            if(action.payload.isFile){
                if(action.payload.name.indexOf(".") >= 0){
                    var name_to_portions = action.payload.name.split(".");
                    var ext_only = name_to_portions[name_to_portions.length - 1];
                    name_to_portions.splice(name_to_portions.length - 1,1);
                    var name_only = name_to_portions.join(".");
                    
                }else{
                    var name_only = action.payload.name;
                    var ext_only = "";
                }
                action.payload.parent.addChildFile({
                    name: name_only,
                    extension: ext_only,
                    creator: action.payload.creator,
                    size: parseInt(action.payload.size),
                    date: action.payload.date
                });
            }else{
                action.payload.parent.addChildFolder({
                    name: action.payload.name,
                    creator: action.payload.creator,
                    date: action.payload.date
                });
            }
            break;
        case "DELETE_OBJECT":
            action.payload.parent.deleteChild(action.payload);
            state = {...state};
            break;
        default:
            break;
    }
    return state;
}