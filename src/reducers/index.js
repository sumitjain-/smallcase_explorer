import {combineReducers} from 'redux';
import filesReducer from './reducer-files';
import currentPathReducer from './reducer-current-path';
import infoObjectReducer from './reducer-info-object';
import contextMenuReducer from './reducer-context-menu';

var allReducers = combineReducers({
    files: filesReducer,
    currentPath: currentPathReducer,
    infoObject: infoObjectReducer,
    contextMenuObject: contextMenuReducer
});

export default allReducers;