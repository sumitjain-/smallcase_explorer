export class Folder{
    constructor(options){

        this.name = options.name || "untitled";
        this.parent = options.parent || null ;
        this.creator = options.creator || "User" ;
        this.date = options.date || new Date() ;
        this.isFile = options.isFile || false;

        if(!options.isFile){
            this.children = [];
        }
    }

    getAddress(){
        var chain = [];
        var me = this;
        if(me.isFile){
            chain.push(`${me.name}.${me.extension}`);
        }else{
            chain.push(me.name);
        }
        
        while(me.parent){
            chain.push(me.parent.name);
            me = me.parent;
        }
        chain.reverse();
        return chain;
    }

    findByName(name){
        if(this.isFile){
            return this.children.find(elem => `${elem.name}.${elem.extension}` == name);
        }else{
            return this.children.find(elem => `${elem.name}` == name);
        }
        
    }

    addChildFile(options){
        options.parent = this;
        var name_distinguisher = 1;
        if(this.children.find(elem => (elem.name == options.name && elem.extension == options.extension))){
            options.name = options.name + " " + name_distinguisher
            while(this.children.find(elem => (elem.name == options.name && elem.extension == options.extension))){
                name_distinguisher++;
                options.name = options.name.split(" ").slice(0, options.name.split(" ")-1).join(" ") + name_distinguisher;
            }
        }
        
        this.children.push(new File(options));
    }
    addChildFolder(options){
        options.parent = this;
        var name_distinguisher = 1;
        this.children.filter(element => !element.isFile).find(elem => (elem.name == options.name));
        if(this.children.filter(element => !element.isFile).find(elem => (elem.name == options.name))){
            options.name = options.name + " " + name_distinguisher
            while(this.children.filter(element => !element.isFile).find(elem => (elem.name == options.name))){
                name_distinguisher++;
                options.name = options.name.split(" ").slice(0, options.name.split(" ")-1).join(" ") + name_distinguisher;
            }
        }
        this.children.push(new Folder(options));
    }

    deleteChild(object){
        var indexOfObj = this.children.findIndex(elem => elem == object);
        this.children[indexOfObj] = null;
        this.children.splice(indexOfObj, 1);
    }
}

export class File extends Folder {
    constructor(options){
        super({
            name: options.name || "untitled",
            creator: options.creator|| "User",
            date: options.date || new Date() ,
            parent: options.parent || null,
            isFile: true
        });
        this.extension = options.extension || "" ;
        this.size = options.size || 0 ;
    }
}