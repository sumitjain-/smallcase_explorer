import React, { Component } from 'react';

import {connect} from 'react-redux';

import Sidebar from './Sidebar';
import Main from './Main';

import {closeContextMenuWindow} from "../actions";

class App extends Component {
    constructor(props){
        super(props);
    }
    render(){
        return (
            <div onClick={this.props.closeContextMenuWindow} id="root">
                <Sidebar />
                <Main />
            </div>
        )
    }
}

function mapStateToProps(state){
    return {
        files: state.files
    }
}

function mapDispatchToProps(dispatch){
    return {
        closeContextMenuWindow: (obj) => dispatch(closeContextMenuWindow(obj))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);