import React, { Component } from 'react';
import {connect} from 'react-redux';

import ContextMenu from './ContextMenu';

import {setCurrentPath, setContextMenuObject} from '../actions';

class FolderThumb extends Component {
    constructor(props){
        super(props);
        this.openThisFolder = this.openThisFolder.bind(this);
        this.contextMenuOpen = this.contextMenuOpen.bind(this);

    }

    contextMenuOpen(e){
        this.props.setContextMenuObject(this.props.element);
        e.preventDefault();
        console.log(this.props.element);
    }

    openThisFolder(){
        this.props.setCurrentPath(this.props.element);
    }
    render(){
        return (
            <div className={["item", "folder", ((this.props.contextMenuObject) && (this.props.element.getAddress().join("/") == this.props.contextMenuObject.getAddress().join("/")) ? "contextMenuActive" : "" )].join(" ")} onDoubleClick={this.openThisFolder}  onContextMenu={this.contextMenuOpen} >
                <div className="icon">
                </div>
                <span>
                    {this.props.element.name}
                </span>
                {
                    this.props.contextMenuObject && 
                    this.props.element == this.props.contextMenuObject ?
                    (<ContextMenu handleDelete={this.props.handleDelete} element={this.props.element} />) : (null)
                }
                
            </div>
        )
    }
}


function mapStateToProps(state){
    return {
        currentPath: state.currentPath,
        contextMenuObject: state.contextMenuObject
    }
}

function mapDispatchToProps(dispatch){
    return {
        setCurrentPath: (obj) => dispatch(setCurrentPath(obj)),
        setContextMenuObject: (obj) => dispatch(setContextMenuObject(obj))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FolderThumb);