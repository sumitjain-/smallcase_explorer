import React, { Component } from 'react';
import { connect } from "react-redux";

import { addObject } from "../actions";

class AddNewForm extends Component {
    constructor(props){
        super(props)
        this.state = {
            fileForm: true
        }

        this.setToFileForm = this.setToFileForm.bind(this);
        this.setToFolderForm = this.setToFolderForm.bind(this);
        this.createObject = this.createObject.bind(this);
        
    }

    createObject(){
        var newObj = {};
        newObj.name = this.refs.name.value || "untitled";
        newObj.creator = this.refs.creator.value;
        if(this.state.fileForm){
            newObj.size = this.refs.size.value;
        }
        newObj.date = this.refs.date.value;
        newObj.parent = this.props.currentPath;
        newObj.isFile = this.state.fileForm;

        console.log(newObj);
        this.props.addObject(newObj);

        Object.keys(this.refs).forEach(function(refName){
            this.refs[refName].value = null;
        }, this);
        this.props.formClose();
    }

    setToFileForm(){
        var tempState = this.state;
        tempState.fileForm = true;
        this.setState(tempState);
    }

    setToFolderForm(){
        var tempState = this.state;
        tempState.fileForm = false;
        this.setState(tempState);
    }

    render(){
        return (
            <div className={(this.props.visible) ? "active": ""} id="addNewForm">
                <div className="inner">
                    <span onClick={this.props.formClose} className="toggleBtn">
                    </span>
                    <h1>
                        Create New
                    </h1>
                    <div className="btnGroup">
                        <button onClick={this.setToFileForm} className={["fileFormBtn", (this.state.fileForm ? "active": "")].join(" ")}>File</button>
                        <button onClick={this.setToFolderForm} className={["folderFormBtn", (this.state.fileForm ? "": "active")].join(" ")}>Folder</button>
                    </div>

                    <div className="formInputs">
                        <input ref="name" type="text" placeholder="Name" />
                        <input ref="creator" type="text" placeholder="Creator" />
                        {
                            this.state.fileForm ?
                            (
                                <input ref="size" type="number" placeholder="Size" />
                            ) : (null)
                        }
                        
                        <input ref="date" type="date" placeholder="Date" />
                        <button onClick={this.createObject} className="addObject">Create</button>
                    </div>

                </div>
            </div>
        )
    }
}

function mapStateToProps(state){
    return {
        currentPath: state.currentPath
    }
}

function mapDispatchToProps(dispatch){
    return {
        addObject: (obj) => dispatch(addObject(obj))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddNewForm);