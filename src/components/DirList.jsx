import React, { Component } from 'react'
import {connect} from 'react-redux';

import FolderDisplay from './FolderDisplay';

class DirList extends Component {
    constructor(props){
        super(props);
    }
    
    render(){
        return (
            <div id="dirList">
                <FolderDisplay element={this.props.files} level={0} />
            </div>
        )
    }
}

function mapStateToProps(state){
    return {
        files: state.files
    }
}

export default connect(mapStateToProps)(DirList);