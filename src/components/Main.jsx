import React, { Component } from 'react';
import {connect} from 'react-redux';

import FileThumb from './FileThumb';
import FolderThumb from './FolderThumb';
import CurrentPathHeader from './CurrentPathHeader';
import AddNewForm from './AddNewForm';
import InfoWindow from "./InfoWindow";

import {deleteObject} from "../actions";

class Main extends Component {
    constructor(props){
        super(props);

        this.state = {
            addNewFormActive: false
        }

        this.render = this.render.bind(this);
        this.addNewFormShow = this.addNewFormShow.bind(this);
        this.addNewFormClose = this.addNewFormClose.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    handleDelete(obj){
        this.props.deleteObject(obj);
        this.render();
    }

    addNewFormShow(){
        var tempState = this.state;
        tempState.addNewFormActive = true;
        
        this.setState(tempState);

        console.log(this.state);
    }

    addNewFormClose(){
        var tempState = this.state;
        tempState.addNewFormActive = false;
        
        this.setState(tempState);

        console.log(this.state);
    }
    render(){
        return (
            <div id="main">
                <CurrentPathHeader />
                <div className="folderContentsDisplay">
                    {
                        this.props.currentPath.children.map(elem =>
                            elem.isFile ?
                            (
                                <FileThumb handleDelete={((el) => this.handleDelete.bind(el)(elem))} key={elem.name+"."+elem.extension} element={elem} />
                            ) :
                            (
                                <FolderThumb handleDelete={((el) => this.handleDelete.bind(el)(elem))} key={elem.name} element={elem} />
                            )
                        )
                    }
                    <div onClick={this.addNewFormShow} className="addNewBtn"></div>
                    <AddNewForm formClose={this.addNewFormClose} visible={this.state.addNewFormActive} />
                    <InfoWindow />
                </div>

            </div>
        )
    }
}

function mapStateToProps(state){
    return {
        files: state.files,
        currentPath: state.currentPath
    }
}

function mapDispatchToProps(dispatch){
    return {
        deleteObject: (obj) => dispatch(deleteObject(obj))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Main);