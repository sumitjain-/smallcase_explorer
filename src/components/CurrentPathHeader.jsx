import React, { Component } from 'react';
import {connect} from 'react-redux';
import {setCurrentPath} from '../actions';

class CurrentPathHeader extends Component {
    constructor(props){
        super(props);
        this.state = {
            searchResults: []
        }
        this.goUpFolder = this.goUpFolder.bind(this);
        this.searchByName = this.searchByName.bind(this);
        this.handleSearchClick = this.handleSearchClick.bind(this);
        this.render = this.render.bind(this);
    }

    goUpFolder(){
        if(this.props.currentPath.parent){
            this.props.setCurrentPath(this.props.currentPath.parent);
        }
        
    }

    handleSearchClick(elem){
        if(elem.parent){
            this.props.setCurrentPath(elem.parent);
        }
        this.refs.search.value = ""
        this.setState({
            searchResults: []
        })
    }

    searchByName(){
        console.log("Searching for: ", this.refs.search.value);
        if(!this.refs.search.value){
            var tempState = this.state;
            tempState.searchResults = [];
            this.setState(tempState);
            return;
        }
        var here = this.props.currentPath;
        if(this.refs.global.checked){
            while(here.parent){
                here = here.parent;
            }
        }
        
        var searchHits = [];
        var traversalQueue = [];
        traversalQueue.push(here);
        while(traversalQueue.length){
            var newItem = traversalQueue.shift();
            if(newItem.name.search(this.refs.search.value) >= 0){
                searchHits.push(newItem);
            }
            if(!newItem.isFile){
                traversalQueue = traversalQueue.concat(newItem.children);
            }
        }
        var tempState = this.state;
        tempState.searchResults = searchHits;
        this.setState(tempState);
    }
    render(){
        var searchHits = this.state.searchResults;
        
        return (
            <div className="currentPathHeader">
                <span className="goUpFolder" onClick={this.goUpFolder} ></span>
                <span>
                {
                    this.props.currentPath.getAddress().map(elem => 
                        (
                            <span key={elem} className="label">
                                <span className="divider">
                                    /
                                </span>
                                {
                                    elem
                                }
                            </span>
                        )
                    )
                }
                </span>
                
                <span className="search">
                    <input onChange={this.searchByName} type="text" ref="search" placeholder="Search for anything"/>
                    <span className="global">
                        <label htmlFor="global">global</label>
                        <input onChange={this.searchByName} type="checkbox" name="global" ref="global" title="Search global" id=""/>
                    </span>
                    {
                        this.refs.search && this.refs.search.value ?
                        (
                            <span onClick={() => {this.refs.search.value = ""; this.searchByName();}} className="clearSearch"></span>
                        ) : (null)
                    }
                    <ul className="searchChildren" >
                    {
                        searchHits.map(elem => 
                            (
                                <li key={elem.name} onClick={() =>this.handleSearchClick(elem)} className="searchChild">
                                    {
                                        "/".concat(elem.getAddress().join("/"))
                                    }
                                </li>
                            )
                        )
                    }
                    </ul>
                </span>

            </div>
        )
    }
}

function mapStateToProps(state){
    return {
        currentPath: state.currentPath
    }
}

function mapDispatchToProps(dispatch){
    return {
        setCurrentPath: (obj) => dispatch(setCurrentPath(obj))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CurrentPathHeader);