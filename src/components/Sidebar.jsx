import React, { Component } from 'react';
import {connect} from 'react-redux';
import DirList from './DirList';

class Sidebar extends Component {
    render(){
        return (
            <div id="sidebar">
                <h1>
                    ROOT
                </h1>
                <DirList />
            </div>
        )
    }
}

function mapStateToProps(state){
    return {
        // files: state.files
    }
}

export default connect(mapStateToProps)(Sidebar);