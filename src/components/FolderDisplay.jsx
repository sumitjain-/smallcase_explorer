import React, { Component } from 'react';

class FolderDisplay extends Component {
    constructor(props){
        super(props);

        this.state = {
            isOpen: false
        }

        this.setActive = this.setActive.bind(this);
        this.setCurrentPath = this.setCurrentPath.bind(this);
    }

    setCurrentPath(){
        
    }

    setActive(){
        var tempState = this.state;

        tempState.isOpen = !tempState.isOpen;

        this.setState(tempState);

        console.log(this.props.element.name);
        console.log(this.state);
    }

    render(){
        if(this.props.element.isFile){
            return (null);
        }else{
            let indents = [];
            for (var i=0; i<this.props.level; i++){
                indents.push(<span key={i} className="indent" ></span>)
            }
            return (
                <div className={["folderBox", (this.state.isOpen ? "active" : "" )].join(" ")} key={this.props.element.name} >
                    <span onClick={this.setActive} key={this.props.element.name} className="fileBar">
                        {
                            indents
                        }
                        
                        <span onClick={this.setCurrentPath} className="folderLabel">
                            {
                                `${this.props.element.name.charAt(0).toUpperCase() + this.props.element.name.slice(1)}`
                            }
                        </span>
                        <span className="arrow"></span>

                    </span>
                    {
                        this.props.element.children.map(elem => <FolderDisplay key={elem.name} element={elem} level={this.props.level+1} />)
                    }
                </div>
            )
        }
    }
}

export default FolderDisplay;