import React, { Component } from 'react';
import { connect } from "react-redux";

import {closeInfoWindow} from '../actions';

class InfoWindow extends Component {
    constructor(props){
        super(props);

        this.windowClose = this.windowClose.bind(this);
    }
    windowClose(){
        this.props.closeInfoWindow();
    }
    render(){
        if(this.props.infoObject){
            return (
                <div className={"active"} id="infoWindow">
                    <div className="inner">
                        <span onClick={this.windowClose} className="toggleBtn">
                            </span>
                            <h1>
                                File Info
                            </h1>
                            {
                                this.props.infoObject.isFile ?
                                (
                                    <div className="icon file">
                                        <span>
                                            {
                                                this.props.infoObject.extension != "" ?
                                                `.${this.props.infoObject.extension}` :
                                                (<em>no ext</em>)
                                            }
                                        </span>
                                        
                                    </div>
                                ) : 
                                (
                                    <div className="icon folder">
                                        
                                    </div>
                                )
                            }
                            
                            <div className="propertiesDisplay">
                                <div className="field">
                                    <span className="label">Name: </span>
                                    {
                                        this.props.infoObject.isFile ?
                                        (
                                            <span className="value">{this.props.infoObject.name}{this.props.infoObject.extension ? `.${this.props.infoObject.extension}`: "" }</span>
                                        ) : 
                                        (
                                            <span className="value">{this.props.infoObject.name}</span>
                                        )
                                    }
                                    
                                </div>
                                {
                                    this.props.infoObject.isFile ?
                                    (
                                        <div className="field">
                                            <span className="label">Size: </span>
                                            <span className="value">{`${this.props.infoObject.size}kb`}</span>
                                        </div>
                                    ) : 
                                    (
                                        null
                                    )
                                }
                                
                                <div className="field">
                                    <span className="label">Creator name: </span>
                                    <span className="value">
                                        {
                                            `${this.props.infoObject.creator[0].toUpperCase()}${this.props.infoObject.creator.length > 1 ? this.props.infoObject.creator.slice(1) : ""}`
                                        }
                                    </span>
                                </div>
                                <div className="field">
                                    <span className="label">Date: </span>
                                    <span className="value">
                                        {`${this.props.infoObject.date.toLocaleString("en-US", {
                                            day: "2-digit",
                                            month: "short",
                                            year: "numeric"
                                        })}`}
                                    </span>
                                </div>
                            </div>
                    </div>
                </div>
            )
        }else{
            return (null)
        }
        
    }
}


function mapStateToProps(state){
    return {
        infoObject: state.infoObject
    }
}

function mapDispatchToProps(dispatch){
    return {
        closeInfoWindow: () => dispatch(closeInfoWindow())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(InfoWindow);