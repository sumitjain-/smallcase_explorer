import React, { Component } from 'react';
import { connect } from "react-redux";
import { setInfoObject, setContextMenuObject, deleteObject} from "../actions";
import ContextMenu from './ContextMenu';

class FileThumb extends Component {
    constructor(props){
        super(props);

        this.openFile = this.openFile.bind(this);
        this.contextMenuOpen = this.contextMenuOpen.bind(this);
    }

    contextMenuOpen(e){
        this.props.setContextMenuObject(this.props.element);
        e.preventDefault();
        console.log(this.props.element);
    }

    openFile(){
        this.props.setInfoObject(this.props.element);
    }

    render(){
        return (
            <div className={["item", "file", ((this.props.contextMenuObject) && (this.props.element.getAddress().join("/") == this.props.contextMenuObject.getAddress().join("/")) ? "contextMenuActive" : "" )].join(" ")} onDoubleClick={this.openFile} onContextMenu={this.contextMenuOpen} >
                <div className="icon">
                    <span>
                        {
                            this.props.element.extension != "" ?
                            `.${this.props.element.extension}` :
                            (<em>no ext</em>)
                        }
                    </span>
                    
                </div>
                <span>
                {this.props.element.name}{
                            this.props.element.extension != "" ?
                            `.${this.props.element.extension}` :
                            ""
                        }
                </span>
                {
                    this.props.contextMenuObject && 
                    this.props.element == this.props.contextMenuObject ?
                    (<ContextMenu handleDelete={this.props.handleDelete} element={this.props.element} />) : (null)
                }
                
            </div>
        )
    }
}

function mapStateToProps(state){
    return {
        currentPath: state.currentPath,
        contextMenuObject: state.contextMenuObject
    }
}

function mapDispatchToProps(dispatch){
    return {
        setInfoObject: (obj) => dispatch(setInfoObject(obj)),
        setContextMenuObject: (obj) => dispatch(setContextMenuObject(obj))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FileThumb);