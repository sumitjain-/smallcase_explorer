import React, { Component } from 'react';
import {connect} from 'react-redux';

import {setCurrentPath, setInfoObject, deleteObject} from '../actions';

class ContextMenu extends Component {
    constructor(props){
        super(props);
        this.clickGetInfo = this.clickGetInfo.bind(this);
        this.clickDelete = this.clickDelete.bind(this);
        this.clickOpen = this.clickOpen.bind(this);
    }

    clickOpen(){
        if(this.props.element.isFile){
            this.props.setInfoObject(this.props.element);
        }else{
            this.props.setCurrentPath(this.props.element);
        }
    }

    clickGetInfo(){
        this.props.setInfoObject(this.props.element);
    }

    clickDelete(){
        this.props.handleDelete();
    }

    render(){
        return (
            <div className="contextMenu">
                <button onClick={this.clickOpen} >Open</button>
                <button onClick={this.clickGetInfo} >Get Info</button>
                <button onClick={this.clickDelete} >Delete</button>
            </div>
        )
    }
}

function mapStateToProps(state){
    return {
        
    }
}

function mapDispatchToProps(dispatch){
    return {
        setInfoObject: (obj) => dispatch(setInfoObject(obj)),
        setCurrentPath: (obj) => dispatch(setCurrentPath(obj)),
        deleteObject: (obj) => dispatch(deleteObject(obj))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ContextMenu)