import React from 'react';
import ReactDOM from 'react-dom';

import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import promise from 'redux-promise';
import {createLogger} from 'redux-logger';
import allReducers from './reducers';

import "./styles/main.scss";

import {File, Folder} from './modules/File';

import App from './components/App';

const logger = createLogger();

var root = new Folder({
    name: "root"
});

root.addChildFolder({
    name: "apps"
})

root.addChildFolder({
    name: "pictures"
})

root.addChildFolder({
    name: "videos"
})

root.addChildFolder({
    name: "docs"
});

root.findByName("docs").addChildFile({
    name: "c",
    extension: "pdf"
})

root.addChildFile({
    name: "a",
    extension: "pdf",
});

root.addChildFile({
    name: "b",
    extension: "jpg",
});

root.findByName("docs").addChildFile({
    name: "d",
    extension: "docx"
})

root.findByName("docs").addChildFolder({
    name: "work"
})

root.findByName("docs").findByName("work").addChildFile({
    name: "e",
    extension: "pdf"
})

root.findByName("docs").findByName("work").addChildFile({
    name: "f",
    extension: "ts"
})

const store = createStore(
    allReducers, {
        files: root,
        currentPath: root
    },
    applyMiddleware(thunk, promise, logger)
);

ReactDOM.render(
    <Provider store={store} >
        <App />
    </Provider>,
    document.getElementById('app')
)